#!/usr/bin/env python

import glob
from pint import UnitRegistry
from math import ceil
import yaml
from fractions import Fraction
import argparse
from jinja2 import Environment, FileSystemLoader, select_autoescape
from os.path import basename
from fuzzywuzzy import fuzz


def recipe_meta(recipe, inc_garnish=False):

    ingredients = [x.get('ingredient', x.get('special')) for x in recipe['ingredients']]
    garnish = recipe.get('garnish')

    if isinstance(garnish, str):
        garnish = [garnish]

    if garnish and inc_garnish:
        ingredients += garnish

    meta = dict(
        name = recipe['name'],
        type = recipe.get('category',''),
        ingredients = list(map(lambda x: x.title(), ingredients))
    )

    return meta


def read_recipe_yaml(files, garnish):

    recipes = []

    if files:
        current_file = files.pop(0)
        print('        ', current_file)
        with open(current_file) as fyaml:
            recipe_dict = yaml.load(fyaml, yaml.Loader)
            recipes += recipe_dict

    if files:
        fulltext = read_recipe_yaml(files, garnish)
        recipes += fulltext

    return recipes


# Helper function for computing covers
def all_covers(sets, size):
    """Return the sets of size no more than `size` that cover as many of `sets` as possible"""
    unique_sets = []
    [unique_sets.append(s) for s in sets if len(s) <= size and s not in unique_sets]
    unions = unique_sets.copy()
    covers = []
    i = 0
    while i < len(unions):
        covers.append(0)
        for s in sets:
            if s.issubset(unions[i]):
                covers[i] += 1
        for s in unique_sets:
            u = unions[i].union(s)
            if len(u) <= size and u not in unions:
                unions.append(u)
        i += 1
    if unions == []:
        return []
    return sorted(unions, key=lambda s: (-covers[unions.index(s)], len(s), list(s)))


def best_covers(sets, size):
    unions = all_covers(sets, size)
    best_unions = []
    lastcover = None
    for u in unions:
        cover = 0
        for s in sets:
            if s.issubset(u):
                cover += 1
        if lastcover == None or cover == lastcover:
            best_unions.append(u)
            lastcover = cover
        else:
            break
    return best_unions


def print_covers(covers):
    n = len(covers[0])
    if n==1:
        plural = ''
    else:
        plural = 'S'
    print("\nIF YOU'RE BUYING", n, "INGREDIENT" + plural + ":")
    for s in covers:
        allows = [recipe['name'] for recipe in incomplete_recipes
                  if all([ing in s for ing in recipe['missing']])]
        print('  (' + str(len(allows)) + ') ' + ', '.join(sorted(s)) + ' - ' + ', '.join(allows))


def analyze_recipes(recipes, show_unkown=False):
    citrus = ['lime', 'lemon', 'orange', 'grapefruit']
    treatments = ['juice', 'twist', 'wedge', 'slice', 'peel', 'zest']

    # Get the ingredients
    ingredients = set()

    recipes = [recipe_meta(x) for x in recipes]

    for recipe in recipes:
        for ing in recipe['ingredients']:
            ingredients.add(ing)

    # Write the ingredients
    f = open('ingredients.out', 'w')
    for ing in sorted(ingredients):
        f.write(ing + '\n')
    f.close()

    # Read the ingredients I have
    try:
        my_ingredients = open('my-ingredients.txt', 'r', encoding='utf-8').read().strip().split('\n')
        my_ingredients = list(map(lambda x: x.title(), my_ingredients))
    except FileNotFoundError:
        my_ingredients = []

    my_ingredients += ['{} {}'.format(x.title(), y.title()) for y in treatments for x in my_ingredients if x.lower() in citrus]
    print('Got', len(my_ingredients), 'ingredients from my-ingredients.txt')

    # Show unknown ingredients
    unknown_ingredients = [i for i in my_ingredients if i not in ingredients]
    if len(unknown_ingredients) and show_unkown> 0:
        print("Found", len(unknown_ingredients), "unknown ingredient(s):",
              ', '.join(unknown_ingredients))

    # Find all valid drinks
    print('Finding recipes you can make...\n')
    drinks = []
    for recipe in recipes:
        recipe['missing'] = {i for i in recipe['ingredients']
                             if i not in my_ingredients}
        if len(recipe['missing']) == 0:
            drinks.append(recipe)

    incomplete_recipes = [recipe for recipe in recipes if len(recipe['missing']) > 0]
    missing_sets = [recipe['missing'] for recipe in incomplete_recipes]

    return (missing_sets, incomplete_recipes, drinks, ingredients)


def convert_units(ing, ufrom, uto, new_units=None):
    ureg = UnitRegistry()

    if new_units:
        [ureg.define(x) for x in new_units]

    if ing.get('amount'):
        cur = float(sum(Fraction(s) for s in str(ing['amount']).split()))

        if ing.get('unit') == 'oz':
            recipe_unit = 'floz'
        else:
            recipe_unit = ing.get('unit')

        if ing.get('unit','NOTHING') == ufrom:
            cur = cur * ureg(recipe_unit)
            cur = cur.to(ureg(uto))
            if cur.magnitude >= 10:
                cur = ceil(cur.magnitude)
            else:
                cur = cur.magnitude
            ing['unit'] = uto

        # Convert back to fraction and limit size of denominator
        frac = Fraction(cur).limit_denominator(4)

        # Whole Numbers
        if frac.denominator == 1:
            ing['amount'] = int(frac)
        # Improper fraction to mixed number
        elif frac.numerator > frac.denominator:
            a = frac.numerator // frac.denominator
            b = frac.numerator % frac.denominator
            ing['amount'] = '{} {}/{}'.format(a, b, frac.denominator)
        else:
            ing['amount'] = str(frac)

    return ing


def is_list(value):
    return isinstance(value, list)


def format_recipe(recipe, fmt):
    # Do some cleanup on badly formatted recipes.
    if isinstance(recipe.get('preparation'), str):
        recipe['preparation'] = [recipe['preparation']]

    if isinstance(recipe.get('garnish'), str):
        recipe['garnish'] = [recipe['garnish']]

    if fmt == 'yml':

        return yaml.dump([recipe], allow_unicode=True, sort_keys=False)
    else:
        env = Environment(
            loader=FileSystemLoader('templates'),
            autoescape=select_autoescape()
        )

        env.filters.update({'is_list': is_list})
        template = env.get_template(fmt)
        return template.render(**recipe)


if __name__ == '__main__':

    # Instantiate the parser
    parser = argparse.ArgumentParser(description='Do stuff with cocktail recipes.')

    # Optional positional argument
    parser.add_argument(
        'command', type=str, nargs='?',
        choices={'analyze':'A Test', 'print':'Print it real good'},
        help='Operation to perform: %(prog)s [%(choices)s]')

    parser.add_argument(
        '-g', '--garnish',
        action='store_true',
        default=False,
        help='Include garnish when analyzing ingredients. Defaults to {%(default)s}'
    )

    parser.add_argument(
        '-u', '--unknown',
        action='store_true',
        default=False,
        help='Show my-ingredients that are not found in any recipe. Defaults to {%(default)s}'
    )

    parser.add_argument(
        '-d', '--recipe-dir',
        default='./recipes',
        metavar='{dir}',
        help='directory to search for recipe files. Defaults to {%(default)s}'
    )

    # Optional argument
    parser.add_argument(
        '-c', '--convert',
        type=str,
        action='append', nargs=2, metavar=('{from}','{to}'),
        help='Convert recipe units.'
    )

    parser.add_argument(
        '-U', '--unit',
        type=str,
        action='append', metavar=('{myunit = 3 * someunit}'),
        help='Add new units. May be used multiple times.'
    )

    parser.add_argument(
        '-n', '--name',
        type=str,
        metavar='{recipe name}',
        help='Filter by recipe name.'
    )

    parser.add_argument(
        '-i', '--ingredient',
        type=str,
        metavar='{ingredient}',
        help='Filter by ingredient name.'
    )

    parser.add_argument(
        '-o', '--output',
        metavar='{filename}',
        help='Output result to a file.'
     )

    formats = ['yml'] + [basename(x) for x in glob.glob('./templates/*')]

    parser.add_argument(
        '-f', '--format',
        choices=formats, default='yml', metavar='{fmt}',
        help='Format for reciep printing. Choose from: [%(choices)s], default: {%(default)s}',
    )

    args = parser.parse_args()

    cookbooks = glob.glob('{}/*.yaml'.format(args.recipe_dir))
    cb_count = len(cookbooks)
    print('Reading recipes directory...')
    recipes = read_recipe_yaml(cookbooks, args.garnish)

    if args.name:
        ftsr = fuzz.token_set_ratio
        search = args.name.lower()

        recipes = [item for item in recipes if ftsr(item['name'].lower(), search) > 50]

    if args.ingredient:
        ftsr = fuzz.token_set_ratio
        search = args.ingredient.lower()

        for i, recipe in enumerate(recipes):
            if not any(ftsr(x.get('ingredient','').lower(), search) > 50 for x in recipe['ingredients']):
                recipes[i] = None

        recipes = [x for x in recipes if x is not None]


    if args.command == 'analyze':
        missing_sets, incomplete_recipes, drinks, ingredients = analyze_recipes(recipes, args.unknown)

        types = {drink['type'] for drink in drinks}

        for categ in sorted(types):
            print(categ.upper() + 'S:')
            for drink in drinks:
                if drink['type'] == categ:
                    print('  ' + drink['name'] + ' - ' + ', '.join(drink['ingredients']))

        missing_one = all_covers(missing_sets, 1)
        missing_two = best_covers(missing_sets, 2)

        try:
            print_covers(missing_one)
        except IndexError:
            pass

        if sorted(missing_one) != sorted(missing_two):
            try:
                print_covers(missing_two)
            except IndexError:
                pass


        print('\nTotal', len(recipes), 'recipes using',
              len(ingredients), 'ingredients from',
              cb_count, 'files')


    elif args.command == 'print':

        if args.convert:
            conversions = args.convert
        else:
            conversions = [[None, None]]

        for pair in conversions:
            out = []
            for r in recipes:
                r['ingredients'] = list(map(
                    lambda x: convert_units(x, pair[0], pair[1], args.unit),
                    r['ingredients']
                ))
                out.append(r)

            recipes = out

        output = '\n'.join([format_recipe(x, args.format) for x in recipes])

        if args.output:
            with open(args.output, 'w') as outfile:
                outfile.write(output)
        else:
            print(output)

