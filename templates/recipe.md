# {{ name }}

{% if category is defined and category|length %}Category : {{ category }}{% endif %}
{% if glass is defined and glass|length %}Glassware : {{ glass }}{% endif %}

## Ingredients
{% for x in ingredients -%}
{% if x.ingredient is defined  -%} * {% if x.amount is defined  -%}{{ x.amount }} {% endif -%}{% if x.unit is defined  -%}{{ x.unit }} {% endif -%}{{ x.ingredient }}{% endif -%}
{% if x.special is defined  -%} * (Optional) {{ x.special }}{% endif %}
{% endfor -%}

{% if preparation is defined and preparation|length %}
## Preparation
{% for x in preparation -%}
 - {{ x }}
{% endfor -%}
{% endif -%}

{% if garnish is defined and garnish|is_list %}
## Garnish
{% for x in garnish -%}
{{ x }}
{% endfor -%}
{% endif -%}